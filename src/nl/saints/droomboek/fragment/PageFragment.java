package nl.saints.droomboek.fragment;

import java.io.IOException;
import java.io.InputStream;

import com.polites.android.GestureImageView;
import com.polites.android.GestureImageView.CustomGestureImageViewListener;

import nl.droomboek.mobile.R;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class PageFragment extends Fragment {

	public static PageFragment newInstance(int pos) {
		PageFragment f = new PageFragment();
        Bundle args = new Bundle();
        args.putInt("position", (pos+1));
        f.setArguments(args);
        return f;
    }
	
	private GestureImageView iv;	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		iv = (GestureImageView) inflater.inflate(R.layout.page, container, false);
		iv.setImageBitmap(getBitmapFromAsset("images/droomboek_"+getArguments().getInt("position") +".png"));
		if(getActivity()instanceof CustomGestureImageViewListener) iv.setCustomListener((CustomGestureImageViewListener) getActivity());
		return iv;
	}
	
	private Bitmap getBitmapFromAsset(String strName) {
        AssetManager assetManager = getActivity().getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if(iv!=null)iv.zoomReset();
	}
}
