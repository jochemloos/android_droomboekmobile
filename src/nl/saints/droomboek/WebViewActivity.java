package nl.saints.droomboek;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import nl.droomboek.mobile.R;

public class WebViewActivity extends Activity{
	
	private WebView webview;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_webview);
		webview = (WebView) findViewById(R.id.webview);
		webview.setWebViewClient(new WebViewClient() {
			
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				startActivity(new Intent(WebViewActivity.this, MainActivity.class));
				return true;
			}
		});
		webview.setVerticalScrollBarEnabled(false);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.loadUrl("file:///android_asset/html/index.html");
		
	}
}
