package nl.saints.droomboek;

import nl.droomboek.mobile.R;
import nl.saints.droomboek.fragment.PageFragment;
import nl.saints.droomboek.view.CustomViewPager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
public class MainActivity extends FragmentActivity implements com.polites.android.GestureImageView.CustomGestureImageViewListener {
	private DroomboekAdapter adapter;
	private CustomViewPager pager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		adapter = new DroomboekAdapter(getSupportFragmentManager());
		pager = (CustomViewPager) findViewById(R.id.pager);
		pager.setOffscreenPageLimit(1);
		pager.setAdapter(adapter);
	}

	class DroomboekAdapter extends FragmentStatePagerAdapter {

		public DroomboekAdapter(FragmentManager fm) {
			super(fm);
		}

		public Fragment getItem(int pos) {
			return PageFragment.newInstance(pos);
		}
		public int getCount() {
			return 91;
		}
		
		public int getItemPosition(Object object) {
			   return POSITION_NONE;
			}
	}

	public void toggleViewPaging(boolean enabled) {
		pager.setPagingEnabled(enabled);
	}
}
